L.SvgLayer = L.Layer.extend({
    options: {},
    initialize: function (svg, options) {
        options = L.setOptions(this, options);
        this._svg = svg;
    },
    _initContainer: function () {
        if(this._container) {
            return;
        }

        this._container = L.DomUtil.create('div', 'leaflet-svg-layer');
        // inserting a node in this hacky manner because there seem to be no good way
        // to do it with DOM api on SVG
        var insertClosingAt = this._svg.lastIndexOf('<');
        this._svg = this._svg.slice(0, insertClosingAt) + '</g></g>' + this._svg.slice(insertClosingAt)
        var insertOpeningAt = this._svg.indexOf('>');
        this._svg = this._svg.slice(0, insertOpeningAt + 1) + '<g id="svg-translate-box"><g id="svg-scale-box">' + this._svg.slice(insertOpeningAt + 1)
        $(this._container).append(this._svg);
        
        var $svg = $(this._container).find('svg');
        var attrs = $svg[0].attributes;
        var i = 0;
        while(i < attrs.length) {
            if(attrs[i].name.toLowerCase() === 'viewbox') {
                var xyhw = attrs[i].value.split(' ');
                this._originalX = parseInt(xyhw[0], 10);
                this._originalY = parseInt(xyhw[1], 10);
                this._originalWidth = parseInt(xyhw[2], 10);
                this._originalHeight = parseInt(xyhw[3], 10);
            }
            i ++;
        }

        if(this._originalHeight === undefined) {
            throw 'No viewBox defined for svg, too bad';
        }

        this.getPane().appendChild(this._container);
        
        this._reset();

        this._map.fire('movestart');
        L.DomUtil.setPosition(this._map._mapPane, new L.Point(this._originalWidth / 4, this._originalHeight / 4));
        this._map.fire('moveend');
    },
    _reset: function(event) {
        var map = this._map;
        var scale = map.getZoom();
        $svg = $(this._container).find('svg');
        $scaleBox = $(this._container).find('#svg-scale-box');
        $translateBox = $(this._container).find('#svg-translate-box');
        $scaleBox.attr('transform', 'scale(' + (scale + 1) + ')');
        var newWidth = this._originalWidth * Math.pow(2, scale);
        var newHeight = this._originalHeight * Math.pow(2, scale);
        $svg.attr('width', newWidth);
        $svg.attr('height', newHeight);
        // var translateX = (this._originalWidth) * Math.pow(2, scale - 1);
        // var translateY = (this._originalHeight) * Math.pow(2, scale - 1);
        var translateY = this._originalY * (scale + 1);
        // var translateY = this._originalY;
        var translateX = this._originalX * (scale + 1);
        // var translateX = this._originalX;
        $translateBox.attr('transform', 'translate(' + (-1 * translateX) + ', ' + (-1 * translateY) + ')')
        // $svg[0].setAttribute('viewBox', this._originalX + ' ' + this._originalY + ' ' + this._originalHeight * Math.pow(2, scale) + ' ' + this._originalWidth * Math.pow(2, scale));
        $svg[0].removeAttribute('viewBox');
        $svg.css('position', 'relative');
        var realWidth = $('#svg-translate-box')[0].getBBox().width;
        var realHeight = $('#svg-translate-box')[0].getBBox().height;
        $svg.css('left', -1 * realWidth / 2);
        $svg.css('top', -1 * realHeight / 2);
    },
    onAdd: function () {
        this._initContainer();
    },
    onRemove: function () {
        $(this._container).remove();
    },
    getEvents: function () {
        return {
            viewreset: this._reset,
        };
    },
    redraw: function () {
        return this;
    },
    bringToFront: function () {
        return this;
    },
    bringToBack: function () {
        return this;
    },
});